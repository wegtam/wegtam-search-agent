/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.common

import java.time.OffsetDateTime

import cats.syntax.all._
import eu.timepit.refined.auto._
import eu.timepit.refined.scalacheck.numeric._
import eu.timepit.refined.scalacheck.string._
import eu.timepit.refined.types.string.NonEmptyString
import sttp.model.Uri

import org.scalacheck.{ Arbitrary, Gen }
import org.scalacheck.Prop._

class SearchResultTest extends munit.DisciplineSuite {

  override def scalaCheckTestParameters = super.scalaCheckTestParameters.withMinSuccessfulTests(100)

  // TODO Find a saner way to generate these.
  private val genAuthorName: Gen[AuthorName] =
    Arbitrary.arbitrary[NonEmptyString].map(s => AuthorName.unsafeFrom(s.toString()))
  private val genDescriptionText: Gen[DescriptionText] =
    Arbitrary.arbitrary[NonEmptyString].map(s => DescriptionText.unsafeFrom(s.toString()))
  private val genImageURL: Gen[Option[Uri]] = for {
    path <- Gen.nonEmptyListOf(scala.util.Random.alphanumeric.take(scala.util.Random.nextInt(16)).mkString)
    pref <- Gen.oneOf(List("ftp", "ftps", "http", "https"))
    ext  <- Gen.oneOf(List(".gif", ".jpg", ".png", ".svg"))
    url  <- Gen.delay(Uri.parse(s"$pref://example.com/${path.mkString("/")}$ext").toOption)
  } yield url
  private val genPublished: Gen[OffsetDateTime] =
    Gen.choose(-30000L, 30000L).map(d => OffsetDateTime.now().plusDays(d))
  private val genPublisherName: Gen[PublisherName] =
    Arbitrary.arbitrary[NonEmptyString].map(s => PublisherName.unsafeFrom(s.toString()))
  private val genPublishingDetails: Gen[PublishingDetails] =
    Arbitrary.arbitrary[NonEmptyString].map(s => PublishingDetails.unsafeFrom(s.toString()))
  private val genSearchEngineName: Gen[SearchEngineName] =
    Arbitrary
      .arbitrary[NonEmptyString]
      .suchThat(_.trim().nonEmpty)
      .map(s => SearchEngineName.unsafeFrom(s.toString().trim()))

  private val genTitleString: Gen[TitleString] =
    Arbitrary.arbitrary[NonEmptyString].map(s => TitleString.unsafeFrom(s.toString()))
  private val genSearchResultURL: Gen[Uri] = for {
    path <- Gen.nonEmptyListOf(scala.util.Random.alphanumeric.take(scala.util.Random.nextInt(16)).mkString)
    pref <- Gen.oneOf(List("ftp", "ftps", "http", "https"))
    failsafe = Uri("https://example.com/")
    baseUrl <- Gen.delay(
      Uri
        .parse(s"$pref://example.com/")
        .toOption
        .getOrElse(failsafe)
    )
    url = baseUrl.addPath(path)
  } yield url

  private val genSearchResult: Gen[SearchResult] =
    for {
      as         <- Gen.listOf(genAuthorName)
      desc       <- Gen.option(genDescriptionText)
      iurl       <- genImageURL
      published  <- Gen.option(genPublished)
      publisher  <- Gen.listOf(genPublisherName)
      pubDetails <- Gen.listOf(genPublishingDetails)
      es         <- Gen.nonEmptyListOf(genSearchEngineName).map(_.distinct)
      title      <- Gen.option(genTitleString)
      url        <- Gen.option(genSearchResultURL)
      number     <- Arbitrary.arbitrary[SearchResultNumber]
    } yield SearchResult(
      authors = as,
      description = desc,
      imageUrl = iurl,
      published = published,
      publisher = publisher,
      publishingDetails = pubDetails,
      searchEngines = es,
      title = title,
      url = url,
      number = number
    )
  private implicit val arbitrarySearchResult: Arbitrary[SearchResult] = Arbitrary(genSearchResult)
  // Our generator for lists creates them with ascending number field.
  private implicit val arbitrarySearchResultList: Arbitrary[List[SearchResult]] = Arbitrary(
    Gen
      .nonEmptyListOf(genSearchResult)
      .map(_.zipWithIndex.map(t => t._1.copy(number = SearchResultNumber.unsafeFrom(t._2))))
  )

  property("#apply must create a correctly initialised minimal SearchResult") {
    forAll(genSearchEngineName, genTitleString, genSearchResultURL) {
      (name: SearchEngineName, title: TitleString, url: Uri) =>
        val expected = SearchResult(
          authors = List.empty,
          description = None,
          imageUrl = None,
          published = None,
          publisher = List.empty,
          publishingDetails = List.empty,
          searchEngines = List(name),
          title = Option(title),
          url = Option(url),
          number = 0
        )
        assertEquals(SearchResult(name, title, url), expected)
    }
  }

  property("#empty must create a correctly initialised empty SearchResult") {
    forAll(genSearchEngineName) { name: SearchEngineName =>
      val expected = SearchResult(
        authors = List.empty,
        description = None,
        imageUrl = None,
        published = None,
        publisher = List.empty,
        publishingDetails = List.empty,
        searchEngines = List(name),
        title = None,
        url = None,
        number = 0
      )
      assertEquals(SearchResult.empty(name), expected)
    }
  }

  property("Ordering[SearchResult] must sort correctly for equal number of search engines") {
    forAll { rs: List[SearchResult] =>
      val seed = rs.map(_.copy(searchEngines = List("foo"))).reverse
      // The generated list is already "numbered" in ascending order.
      assertEquals(seed.sorted.map(_.url), (rs.map(_.url))) // compare only url to avoid datetime comparison pitfalls
    }
  }

  property("Ordering[SearchResult] must sort correctly for equal number field") {
    forAll { rs: List[SearchResult] =>
      val seed = rs.map(_.copy(number = 0))
      val expected = rs.sortWith { (l, r) =>
        l.searchEngines.size > r.searchEngines.size
      }
      assertEquals(
        seed.sorted.map(_.url),
        (expected.map(_.url))
      ) // compare only url to avoid datetime comparison pitfalls
    }
  }

  property("#combine must be associative") {
    forAll { (a: SearchResult, b: SearchResult, c: SearchResult) =>
      a.combine(b.combine(c)) === c.combine(a.combine(b))
    }
  }

  property("#combine must create correctly combined instances") {
    forAll { (a: SearchResult, b: SearchResult) =>
      val expected = a.copy(
        authors = (a.authors |+| b.authors).distinct,
        description = if (b.description.nonEmpty) b.description else a.description,
        imageUrl = if (b.imageUrl.nonEmpty) b.imageUrl else a.imageUrl,
        published = if (b.published.nonEmpty) b.published else a.published,
        publisher = (a.publisher |+| b.publisher).distinct,
        publishingDetails = (a.publishingDetails |+| b.publishingDetails).distinct,
        searchEngines = (a.searchEngines |+| b.searchEngines).distinct,
        title = if (b.title.nonEmpty) b.title else a.title,
        url = if (b.url.nonEmpty) b.url else a.url,
        number = if (a.number < b.number) a.number else b.number
      )
      val combined = a.combine(b)
      assertEquals(combined, expected)
    }
  }

}
