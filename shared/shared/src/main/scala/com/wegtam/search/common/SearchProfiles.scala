/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.common

import eu.timepit.refined.auto._

/** A container for defined [[SearchProfile]] entries.
  *
  * @param profiles
  *   A map of the profile names with their corresponding definitions.
  */
final case class SearchProfiles(profiles: Map[SearchProfileName, SearchProfile])

object SearchProfiles {
  final val CONFIG_KEY: ConfigKey = "profiles"
}
