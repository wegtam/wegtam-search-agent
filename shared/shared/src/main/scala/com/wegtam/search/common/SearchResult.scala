/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.common

import java.time.OffsetDateTime

import cats._
import cats.syntax.all._
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import io.circe._
import io.circe.refined._
import sttp.model.Uri

/** A container for the data we collect for each search result.
  *
  * A search result implements a semigroup which allows us to combine several results into one using the cats syntax `a
  * |+| b |+| c`. This is useful to combine results which are equal but come from different search engines. While all
  * fields are combined using common semigroups there are exceptions. Notably the following fields are not combined but
  * overridden by their "right" counterpart in the equation of that is defined:
  *
  *   1. `imageUrl` 2. `published` 3. `url` 4. `number`
  *
  * Because combining datetimes or urls in general doesn't make sense.
  *
  * @param authors
  *   A list of author names (e.g. writers of a blog post or paper) if feasible which may be empty.
  * @param description
  *   A textual description of the content which usually equals the teaser text shown by search engines.
  * @param imageUrl
  *   An optional URL pointing to an image related to the result.
  * @param published
  *   An optional publishing date and time.
  * @param publisher
  *   A list of publisher names (e.g. publishing houses for books or papers) which might be empty.
  * @param publishingDetails
  *   A list of details about the published item e.g. a science magazine issue with page numbers or an ISBN.
  * @param searchEngines
  *   A list of search engine names which returned this result.
  * @param title
  *   A short summary of the search result (i.e. a title).
  * @param url
  *   The URL pointing to the actual result which is usually a website.
  * @param number
  *   The "number" represents the position of it in the returned result set of a search engine and thus reflects the
  *   "importance", the lower the better.
  */
final case class SearchResult(
    authors: List[AuthorName],
    description: Option[DescriptionText],
    imageUrl: Option[Uri],
    published: Option[OffsetDateTime],
    publisher: List[PublisherName],
    publishingDetails: List[PublishingDetails],
    searchEngines: List[SearchEngineName],
    title: Option[TitleString],
    url: Option[Uri],
    number: SearchResultNumber
) {

  /** Combine the search result with the given one.
    *
    * The general rule of thumb is to never override existing values except if they are empty.
    *
    * TODO Move to a typeclass if it has laws that can be checked.
    *
    * @param y
    *   Another search result.
    * @return
    *   The combined version of this and the given search result.
    */
  def combine(y: SearchResult): SearchResult =
    SearchResult(
      authors = (this.authors |+| y.authors).distinct,
      description = if (y.description.nonEmpty) y.description else this.description,
      imageUrl = if (y.imageUrl.nonEmpty) y.imageUrl else this.imageUrl,
      published = if (y.published.nonEmpty) y.published else this.published,
      publisher = (this.publisher |+| y.publisher).distinct,
      publishingDetails = (this.publishingDetails |+| y.publishingDetails).distinct,
      searchEngines = (this.searchEngines |+| y.searchEngines).distinct,
      title = if (y.title.nonEmpty) y.title else this.title,
      url = if (y.url.nonEmpty) y.url else this.url,
      number = if (this.number < y.number) this.number else y.number // Prefer the lower number.
    )

}

object SearchResult {

  // The default ordering for search results is based upon the number of search engines that returned
  // the result. If these numbers are equal then the weight of the results is compared.
  implicit val orderingSearchResult: Ordering[SearchResult] = new Ordering[SearchResult] {
    override def compare(x: SearchResult, y: SearchResult): Int =
      x.searchEngines.size.compare(y.searchEngines.size) match {
        case 0 => x.number.compare(y.number)
        case d => (-1 * d)
      }
  }
  implicit val orderSearchResult: Order[SearchResult] = Order.fromOrdering[SearchResult]

  implicit val encodeCirce: Encoder[SearchResult] = Encoder.forProduct10(
    "authors",
    "description",
    "imageUrl",
    "published",
    "publisher",
    "publishingDetails",
    "searchEngines",
    "title",
    "url",
    "number"
  )(r =>
    (
      r.authors,
      r.description,
      r.imageUrl,
      r.published,
      r.publisher,
      r.publishingDetails,
      r.searchEngines,
      r.title,
      r.url,
      r.number
    )
  )

  /** Create a minimum search result.
    *
    * @param name
    *   The name of the search engine that produced the result.
    * @param title
    *   A short summary of the search result (i.e. a title).
    * @param url
    *   The URL pointing to the actual result which is usually a website.
    * @return
    *   A search result with the given attributes and the default weight of `0`.
    */
  def apply(name: SearchEngineName, title: TitleString, url: Uri): SearchResult =
    SearchResult(
      authors = List.empty,
      description = None,
      imageUrl = None,
      published = None,
      publisher = List.empty,
      publishingDetails = List.empty,
      searchEngines = List(name),
      title = Option(title),
      url = Option(url),
      number = 0
    )

  /** Create a completly empty search result.
    *
    * @param name
    *   The name of the search engine that produced the result.
    * @return
    *   A search result with the given search engine name and the default weight of `0`.
    */
  def empty(name: SearchEngineName): SearchResult =
    SearchResult(
      authors = List.empty,
      description = None,
      imageUrl = None,
      published = None,
      publisher = List.empty,
      publishingDetails = List.empty,
      searchEngines = List(name),
      title = None,
      url = None,
      number = 0
    )

  implicit val publishedSemigroup: Semigroup[Option[OffsetDateTime]] = new Semigroup[Option[OffsetDateTime]] {
    override def combine(x: Option[OffsetDateTime], y: Option[OffsetDateTime]): Option[OffsetDateTime] =
      (x, y) match {
        case (None, None)    => None
        case (Some(a), None) => Option(a)
        case (_, Some(b))    => Option(b)
      }
  }

}
