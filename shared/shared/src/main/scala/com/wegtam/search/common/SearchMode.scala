/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.common

import cats._
import cats.syntax.all._

/** All supported search modes which are needed to instrument special modes of search engines.
  *
  * The `GENERIC` mode should be used as the default "web search" mode.
  */
sealed trait SearchMode extends Product with Serializable

object SearchMode extends {
  private val mappings: Map[String, SearchMode] = Map(
    "GENERIC" -> GENERIC,
    "FILES"   -> FILES,
    "IMAGES"  -> IMAGES,
    "VIDEO"   -> VIDEO
  )

  implicit val eqSearchMode: Eq[SearchMode] = Eq.instance[SearchMode] { (a, b) =>
    (a, b) match {
      case (GENERIC, GENERIC) => true
      case (FILES, FILES)     => true
      case (IMAGES, IMAGES)   => true
      case (VIDEO, VIDEO)     => true
      case _                  => false
    }
  }

  implicit val showSearchMode: Show[SearchMode] =
    Show.show(m => mappings.find(_._2 === m).map(_._1).getOrElse(m.getClass().getSimpleName()))

  val values = mappings.values.toList

  /** Return either the `SearchMode` with the given name or an error message.
    *
    * @param name
    *   The name of a search mode.
    * @return
    *   Either the search mode or an error message.
    */
  def withNameEither(name: String): Either[String, SearchMode] =
    mappings.get(name) match {
      case Some(m) => Right(m)
      case None    => Left(s"No such SearchMode: $name")
    }

  /** Return an option to `SearchMode` with the given name.
    *
    * @param name
    *   The name of a search mode.
    * @return
    *   An option to the desired search mode which may be none.
    */
  def withNameOption(name: String): Option[SearchMode] = mappings.get(name)

  /** As the name suggest this is intended to be used for generic aka "web" searches.
    */
  case object GENERIC extends SearchMode
  case object FILES   extends SearchMode
  case object IMAGES  extends SearchMode
  case object VIDEO   extends SearchMode
}
