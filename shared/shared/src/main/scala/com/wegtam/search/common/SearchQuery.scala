/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.common

import com.wegtam.countries._
import eu.timepit.refined.types.numeric.PosInt

/** Simple wrapper for search queries.
  *
  * @param query
  *   The query string to pass into a search engine.
  * @param region
  *   The region into which the search should be restricted if possible.
  * @param results
  *   The desired number of results.
  */
final case class SearchQuery(query: String, region: Option[Alpha2CountryCode], results: PosInt)
