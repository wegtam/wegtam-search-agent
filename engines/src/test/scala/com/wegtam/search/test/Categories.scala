/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.test

object Categories {

  val OfflineTest = new munit.Tag("OfflineTest")
  val OnlineTest  = new munit.Tag("OnlineTest")

}
