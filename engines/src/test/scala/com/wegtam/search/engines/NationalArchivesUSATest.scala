/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.engines

import cats.effect._
import com.wegtam.search.common.SearchEngineOutput
import com.wegtam.search.common.SearchMode
import com.wegtam.search.common.SearchQuery
import com.wegtam.search.test.Categories._
import eu.timepit.refined.auto._
import sttp.client3.asynchttpclient.fs2.AsyncHttpClientFs2Backend

class NationalArchivesUSATest extends munit.CatsEffectSuite {
  val resultsFile = ResourceSuiteLocalFixture(
    "search-results-file",
    Resource.make(
      IO.blocking(
        scala.io.Source
          .fromInputStream(
            getClass().getClassLoader().getResourceAsStream("com/wegtam/search/engines/NationalArchivesUSA.html"),
            "UTF-8"
          )
          .mkString
      )
    )(_ => IO.unit)
  )

  override def munitFixtures = List(resultsFile)

  test("must parse results correctly".tag(OfflineTest)) {
    SearchEngineOutput.from(resultsFile()) match {
      case Left(_) => fail("No valid search engine output in test file!")
      case Right(o) =>
        val engine  = new NationalArchivesUSA[IO]
        val parser  = engine.parser
        val results = parser.parseResults(None)(engine.name)(SearchMode.GENERIC)(o)
        results.map(_.size).assertEquals(20)
      // FIXME Check all parsed results!
    }
  }
}

class NationalArchivesUSAOnlineTest extends munit.CatsEffectSuite {
  val sttpBackend = ResourceSuiteLocalFixture("sttp-backend", AsyncHttpClientFs2Backend.resource[IO]())

  override def munitFixtures = List(sttpBackend)

  test("must search online".tag(OnlineTest)) {
    val engine = new NationalArchivesUSA[IO]
    val query  = SearchQuery("henry david thoreau", region = None, results = 20)
    engine.search(query)(sttpBackend()).compile.toList.map(_.size).map(s => assert(s > 0))
  }

  test("must search online with paging".tag(OnlineTest)) {
    val engine = new NationalArchivesUSA[IO]
    val query  = SearchQuery("henry david thoreau", region = None, results = 40)
    engine.search(query)(sttpBackend()).compile.toList.map(_.size).map(s => assert(s > 20))
  }

}
