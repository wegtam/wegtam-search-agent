/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.engines

import cats.effect._
import com.wegtam.countries.Country
import com.wegtam.search.common.SearchEngineOutput
import com.wegtam.search.common.SearchMode
import com.wegtam.search.common.SearchQuery
import com.wegtam.search.test.Categories._
import eu.timepit.refined.auto._
import sttp.client3.asynchttpclient.fs2.AsyncHttpClientFs2Backend
import sttp.model.Uri

class YahooTest extends munit.CatsEffectSuite {
  val resultsFile = ResourceSuiteLocalFixture(
    "search-results-file",
    Resource.make(
      IO.blocking(
        scala.io.Source
          .fromInputStream(
            getClass().getClassLoader().getResourceAsStream("com/wegtam/search/engines/Yahoo.html"),
            "UTF-8"
          )
          .mkString
      )
    )(_ => IO.unit)
  )

  override def munitFixtures = List(resultsFile)

  test("must parse results correctly".tag(OfflineTest)) {
    SearchEngineOutput.from(resultsFile()) match {
      case Left(_) => fail("No valid search engine output in test file!")
      case Right(o) =>
        val engine  = new Yahoo[IO]
        val parser  = engine.parser
        val results = parser.parseResults(None)(engine.name)(SearchMode.GENERIC)(o)
        results.map(_.size).assertEquals(6)
      // FIXME Check all parsed results!
    }
  }

  test("createQueryUri must produce correct urls".tag(OfflineTest)) {
    val engine = new YahooTestEngine
    for {
      c <- Country.values.toList
      p   = scala.util.Random.nextInt(50)
      q   = SearchQuery(scala.util.Random.alphanumeric.take(16).mkString, Option(c.alpha2), 5)
      uri = engine.createQueryUri(q)(p)
    } yield {
      assert(uri.toString().contains(s"p=${q.query}"), "Query parameter not set!")
      assert(uri.toString().contains(s"b=${(p * 7).toString()}"), "Pagination parameter not set!")
      assert(uri.toString().contains(s"vl=lang_${c.alpha2.toString()}"), "Language parameter not set!")
    }
  }
}

class YahooOnlineTest extends munit.CatsEffectSuite {
  val queries = List("category theory", "cognitive computing", "higgs boson", "hilbert space", "particle plasma")
  val searchQuery = ResourceSuiteLocalFixture(
    "search-query",
    Resource.make(IO.delay(queries(scala.util.Random.nextInt(queries.length))))(_ => IO.unit)
  )
  val sttpBackend = ResourceSuiteLocalFixture("sttp-backend", AsyncHttpClientFs2Backend.resource[IO]())

  override def munitFixtures = List(searchQuery, sttpBackend)

  test("must search online".tag(OnlineTest)) {
    val engine = new Yahoo[IO]
    val query  = SearchQuery(searchQuery(), region = None, results = 7)
    engine.search(query)(sttpBackend()).compile.toList.map(_.size).map(s => assert(s > 0))
  }

  test("must search online with paging".tag(OnlineTest)) {
    val engine = new Yahoo[IO]
    val query  = SearchQuery(searchQuery(), region = None, results = 14)
    engine.search(query)(sttpBackend()).compile.toList.map(_.size).map(s => assert(s > 7))
  }

}

class YahooTestEngine extends Yahoo[IO] {
  override def createQueryUri(q: SearchQuery)(page: Int): Uri = super.createQueryUri(q)(page)
}
