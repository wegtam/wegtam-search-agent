/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.engines

import cats.effect._
import cats.syntax.all._
import com.wegtam.search.common._
import com.wegtam.search.engines.SearchEngineParserPatternType._
import eu.timepit.refined.auto._
import eu.timepit.refined.types.numeric.PosInt
import org.jsoup._
import org.jsoup.nodes._
import org.jsoup.select.Elements
import org.slf4j.LoggerFactory
import sttp.model.Uri

import scala.jdk.CollectionConverters._

/** The basic definition of a parser used to convert the raw output of a search engine into a collection of results.
  *
  * Each search engine is supposed to provide a custom class instance for this.
  *
  * @tparam F
  *   A higher kinded type which is usually an applicative or monad in the concrete application.
  */
abstract class SearchEngineParser[F[_]: Sync] {
  protected val log = LoggerFactory.getLogger(getClass())

  /** The "parser configuration" *read* a map of patterns used to extract different parts of a search result grouped
    * after `SearchMode`.
    *
    * @return
    *   A map of patterns used to extract specific parts of a search result for each supported `SearchMode`.
    */
  protected def patterns: Map[SearchMode, Map[SearchEngineParserPatternType, SearchEngineParserPattern]]

  /** Parse the given search engine output and return the parsed search results.
    *
    * @param baseUri
    *   The base URI for the document that shall be parsed.
    * @param name
    *   The name of the search engine.
    * @param m
    *   The desired search mode which affects the kind of parsing.
    * @param data
    *   The output of a search engine, usually HTML code.
    * @return
    *   A list of extracted results which may be empty.
    */
  def parseResults(
      baseUri: Option[String]
  )(name: SearchEngineName)(m: SearchMode)(data: SearchEngineOutput): F[List[SearchResult]] = {
    val getPattern: SearchEngineParserPatternType => Option[SearchEngineParserPattern] = t =>
      patterns.get(m).flatMap(_.get(t))
    for {
      _   <- Sync[F].delay(log.trace("Parsing HTML fragment: {}", data))
      doc <- Sync[F].delay(Jsoup.parse(data))
      _   <- Sync[F].delay(baseUri.foreach(doc.setBaseUri))
      _   <- Sync[F].delay(log.trace("Parsed HTML body: {}", doc.body()))
      sel <- Sync[F].delay(getPattern(EXTRACT_RESULT))
      _   <- Sync[F].delay(log.debug("Using extract result selector: {}", sel))
      es  <- Sync[F].delay(sel.fold(List.empty[Element])(s => doc.select(s).iterator.asScala.toList))
      _   <- Sync[F].delay(log.debug("Extracted {} results.", es.length))
      pts <- Sync[F].delay(SearchEngineParserPatternType.values.filterNot(_ === EXTRACT_RESULT))
      res <- Sync[F].delay {
        es.zipWithIndex
          .map { t =>
            val (e, index) = t
            val number     = SearchResultNumber.from(index).toOption.getOrElse(0: SearchResultNumber)
            val r          = SearchResult.empty(name).copy(number = number)
            val ret = pts.map { pt =>
              pt match {
                case EXTRACT_RESULT_DESCRIPTION =>
                  r.copy(description =
                    getPattern(pt)
                      .flatMap(s => safeFirst(e.select(s)).map(_.text()))
                      .flatMap(d => DescriptionText.from(d).toOption)
                  )
                case EXTRACT_RESULT_IMAGE_URL =>
                  r.copy(imageUrl =
                    getPattern(pt)
                      .flatMap(s => safeFirst(e.select(s)).map(_.absUrl("src")))
                      .flatMap(u => Uri.parse(u).toOption)
                  )
                case EXTRACT_RESULT_TITLE =>
                  r.copy(title =
                    getPattern(pt)
                      .flatMap(s => safeFirst(e.select(s)).map(_.text()))
                      .flatMap(t => TitleString.from(t).toOption)
                  )
                case EXTRACT_RESULT_URL =>
                  r.copy(url =
                    getPattern(pt)
                      .flatMap(s => safeFirst(e.select(s)).map(_.absUrl("href")))
                      .flatMap(u => Uri.parse(u).toOption)
                  )
                case EXTRACT_RESULT => r
              }
            }
            log.trace("Extracted result {}: {}", number, ret)
            ret
          }
          .foldLeft(List.empty[SearchResult])((acc, rs) =>
            // We initialise with `MaxValue` to avoid zeroing out all gathered numbers.
            rs.fold(SearchResult.empty(name).copy(number = PosInt.MaxValue))((ar, r) => ar.combine(r)) :: acc
          )
      }
    } yield res
  }

  /** Simple helper function to safely extract the first element of an elements collection returned by jsoup select
    * operations.
    *
    * @todo
    *   Refactor into typeclass using `headOption`.
    *
    * @param es
    *   A collection of elements which might be empty.
    * @return
    *   An option to the first element.
    */
  @SuppressWarnings(Array("scalafix:DisableSyntax.null", "org.wartremover.warts.Null"))
  private def safeFirst(es: Elements): Option[Element] =
    if (es.first() =!= null)
      Option(es.first())
    else
      None
}
