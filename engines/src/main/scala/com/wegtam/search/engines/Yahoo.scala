/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.engines

import cats.data._
import cats.effect._
import cats.syntax.all._
import com.wegtam.search.common._
import com.wegtam.search.engines.SearchEngineParserPatternType._
import eu.timepit.refined.auto._
import fs2.Stream
import org.slf4j.LoggerFactory
import sttp.client3._
import sttp.model.Uri

import scala.concurrent.duration._

class Yahoo[F[_]: Sync] extends SearchEngine[F] {
  protected val log = LoggerFactory.getLogger(getClass())

  private final val BASE_URL                 = "https://search.yahoo.com"
  private final val DEFAULT_RESULTS_PER_PAGE = 7
  private final val PARAMETER_NAME_AGE       = "age"
  private final val PARAMETER_NAME_ENCODING  = "ei"
  private final val PARAMETER_NAME_LANGUAGE  = "vl"
  private final val PARAMETER_NAME_PAGING    = "b"
  private final val PARAMETER_NAME_QUERY     = "p"
  private final val PARAMETER_NAME_TIME      = "btf"

  /** Create the URI which will be used to query the search engine for information.
    *
    * @param q
    *   The search query to be passed to the engine.
    * @param page
    *   The pagination parameter value needed to page through the results.
    * @return
    *   The URI to be used to query the search engine.
    */
  protected def createQueryUri(q: SearchQuery)(page: Int): Uri = {
    val lang = q.region.map(code => s"lang_${code.toString()}")
    uri"$BASE_URL/search?$PARAMETER_NAME_QUERY=${q.query}&$PARAMETER_NAME_PAGING=${page * DEFAULT_RESULTS_PER_PAGE}&$PARAMETER_NAME_ENCODING='UTF-8'&$PARAMETER_NAME_LANGUAGE=$lang"
  }

  override val capabilities: NonEmptyList[SearchEngineCapabilities] =
    NonEmptyList.of(
      SearchEngineCapabilities.Paging,
      SearchEngineCapabilities.RegionalSearch,
      SearchEngineCapabilities.TimeFrameSearch
    )

  override val modes: NonEmptyList[SearchMode] = NonEmptyList.of(SearchMode.GENERIC)

  override val name: SearchEngineName = Yahoo.ENGINE_NAME

  override val parser: SearchEngineParser[F] = new SearchEngineParser[F] {
    override protected val patterns: Map[SearchMode, Map[SearchEngineParserPatternType, SearchEngineParserPattern]] =
      Map(
        SearchMode.GENERIC -> Map(
          EXTRACT_RESULT             -> ".searchCenterMiddle div.algo-sr",
          EXTRACT_RESULT_DESCRIPTION -> ".compText",
          EXTRACT_RESULT_TITLE       -> "h3.title",
          EXTRACT_RESULT_URL         -> "h3.title a[href]"
        )
      )
  }

  override def search(q: SearchQuery)(implicit
      backend: sttp.client3.SttpBackend[F, sttp.capabilities.fs2.Fs2Streams[F] with sttp.capabilities.WebSockets]
  ): Stream[F, SearchResult] = {
    val createPageQuery: Int => Uri = createQueryUri(q)(_)
    val requests = Stream.emits((0 to q.results / DEFAULT_RESULTS_PER_PAGE).map { page =>
      val request = basicRequest
        .get(createPageQuery(page))
        .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:77.0) Gecko/20100101 Firefox/77.0")
        .readTimeout(FiniteDuration(30, SECONDS))
        .response(asStringAlways.map(SearchEngineOutput.from))
      log.debug("Prepared request to Yahoo: {}", request)
      request
    })
    val parse = parser.parseResults(BASE_URL.some)(name)(SearchMode.GENERIC)(_)
    val results = requests
      .evalMap(_.send(backend))
      .evalMap(_.body.traverse(parse))
      .flatMap(r => Stream.emits(r.getOrElse(List.empty)))
      .take(q.results.toLong)
    results
  }

}

object Yahoo {
  val ENGINE_NAME: SearchEngineName = "Yahoo"
}
