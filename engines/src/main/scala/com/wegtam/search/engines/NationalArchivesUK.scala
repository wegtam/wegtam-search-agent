/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.engines

import cats.data._
import cats.effect._
import cats.syntax.all._
import com.wegtam.search.common._
import com.wegtam.search.engines.SearchEngineParserPatternType._
import eu.timepit.refined.auto._
import fs2.Stream
import sttp.client3._

import scala.concurrent.duration._

class NationalArchivesUK[F[_]: Sync] extends SearchEngine[F] {
  private final val BASE_URL                 = "https://nationalarchives.gov.uk"
  private final val DEFAULT_RESULTS_PER_PAGE = 15

  override val capabilities: NonEmptyList[SearchEngineCapabilities] = NonEmptyList.of(SearchEngineCapabilities.Paging)

  override val modes: NonEmptyList[SearchMode] = NonEmptyList.of(SearchMode.GENERIC)

  override val name: SearchEngineName = NationalArchivesUK.ENGINE_NAME

  override val parser: SearchEngineParser[F] = new SearchEngineParser[F] {
    override protected val patterns: Map[SearchMode, Map[SearchEngineParserPatternType, SearchEngineParserPattern]] =
      Map(
        SearchMode.GENERIC -> Map(
          EXTRACT_RESULT             -> ".searchList li",
          EXTRACT_RESULT_DESCRIPTION -> "p",
          EXTRACT_RESULT_TITLE       -> "h3",
          EXTRACT_RESULT_URL         -> "h3 a[href]"
        )
      )
  }

  override def search(q: SearchQuery)(implicit
      backend: sttp.client3.SttpBackend[F, sttp.capabilities.fs2.Fs2Streams[F] with sttp.capabilities.WebSockets]
  ): Stream[F, SearchResult] = {
    val requests = Stream.emits((0 to q.results / DEFAULT_RESULTS_PER_PAGE).map { page =>
      basicRequest
        .get(uri"$BASE_URL/search/results/${page + 1}?_q=${q.query}")
        .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:77.0) Gecko/20100101 Firefox/77.0")
        .readTimeout(FiniteDuration(30, SECONDS))
        .response(asStringAlways.map(SearchEngineOutput.from))
    })
    val parse = parser.parseResults(BASE_URL.some)(name)(SearchMode.GENERIC)(_)
    val results = requests
      .evalMap(_.send(backend))
      .evalMap(_.body.traverse(parse))
      .flatMap(r => Stream.emits(r.getOrElse(List.empty)))
      .take(q.results.toLong)
    results
  }

}

object NationalArchivesUK {
  val ENGINE_NAME: SearchEngineName = "National Archives of the UK"
}
