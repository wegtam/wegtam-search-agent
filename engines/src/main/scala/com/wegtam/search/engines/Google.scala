/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.engines

import cats.data._
import cats.effect._
import cats.syntax.all._
import com.wegtam.search.common._
import com.wegtam.search.engines.SearchEngineParserPatternType._
import eu.timepit.refined.auto._
import fs2.Stream
import sttp.client3._

import scala.concurrent.duration._

class Google[F[_]: Sync] extends SearchEngine[F] {
  private final val BASE_URL                         = "https://www.google.com"
  private final val PARAMETER_NAME_LANGUAGE          = "hl"
  private final val PARAMETER_NAME_NUMBER_OF_RESULTS = "num"
  private final val PARAMETER_NAME_PAGING            = "start"
  private final val DEFAULT_RESULTS_PER_PAGE         = 10

  override val capabilities: NonEmptyList[SearchEngineCapabilities] =
    NonEmptyList.of(
      SearchEngineCapabilities.LanguageSearch,
      SearchEngineCapabilities.Paging,
      SearchEngineCapabilities.TimeFrameSearch
    )

  override val modes: NonEmptyList[SearchMode] = NonEmptyList.of(SearchMode.GENERIC)

  override val name: SearchEngineName = Google.ENGINE_NAME

  override val parser: SearchEngineParser[F] = new SearchEngineParser[F] {
    override protected val patterns: Map[SearchMode, Map[SearchEngineParserPatternType, SearchEngineParserPattern]] =
      Map(
        SearchMode.GENERIC -> Map(
          EXTRACT_RESULT       -> "#rso div.g",
          EXTRACT_RESULT_TITLE -> "a[href] h3",
          EXTRACT_RESULT_URL   -> "a[href]"
        )
      )
  }

  override def search(q: SearchQuery)(implicit
      backend: sttp.client3.SttpBackend[F, sttp.capabilities.fs2.Fs2Streams[F] with sttp.capabilities.WebSockets]
  ): Stream[F, SearchResult] = {
    val results = for {
      resp <- basicRequest
        .get(uri"$BASE_URL/search?q=${q.query}&$PARAMETER_NAME_NUMBER_OF_RESULTS=${q.results}")
        .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:77.0) Gecko/20100101 Firefox/77.0")
        .readTimeout(FiniteDuration(30, SECONDS))
        .response(asStringAlways.map(SearchEngineOutput.from))
        .send(backend)
      parsed <- resp.body.traverse(o => parser.parseResults(BASE_URL.some)(name)(SearchMode.GENERIC)(o))
    } yield parsed.getOrElse(List.empty).take(q.results)
    Stream.evalSeq(results)
  }

}

object Google {
  val ENGINE_NAME: SearchEngineName = "Google"
}
