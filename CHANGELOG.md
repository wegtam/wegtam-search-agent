# ChangeLog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## Conventions when editing this file.

Please follow the listed conventions when editing this file:

- one subsection per version
- reverse chronological order (latest entry on top)
- write all dates in iso notation (`YYYY-MM-DD`)
- each version should group changes according to their impact:
    - `Added` for new features.
    - `Changed` for changes in existing functionality.
    - `Deprecated` for once-stable features removed in upcoming releases.
    - `Removed` for deprecated features removed in this release.
    - `Fixed` for any bug fixes.
    - `Security` to invite users to upgrade in case of vulnerabilities.

## Unreleased

## 0.2.1 (2022-01-02)

### Added

- HTML format for CLI output
- JSON format for CLI output

## 0.2.0 (2021-12-31)

### Changed

- rename modules for cleaner artefact names

## 0.1.1 (2021-12-31)

### Added

- license information for publishing

## 0.1.0 (2021-12-31)

- initial release

