# Wegtam Search Agent #

![Sonatype Nexus (Releases)](https://img.shields.io/nexus/r/com.wegtam/search-agent-shared_2.13?server=https%3A%2F%2Foss.sonatype.org)

Your personal search agent for the information age.

The Wegtam Search Agent provides a meta search upon a variety of search
engines and aggregates their results.

## Getting the CLI

Currently the fastest way to get the command line version running is to
bootstrap it via [Coursier](https://get-coursier.io/):

```txt
% coursier bootstrap com.wegtam::search-agent-cli:VERSION -o ~/.local/bin/wsa-cli
% hash -r
% wsa-cli --help
Usage: Wegtam Search Agent [--list-engines] [--list-formats] [--list-profiles] [--engine <engine-name>]... [--format <format-name>] [--profile <profile-name>] [--query <string>] [--results <integer>]

CLI interface for the Wegtam Search Agent meta search engine.

Options and flags:
...
```

Please replace the `VERSION` in the command with the version you want to
install.

**Note that it might take some time until a release hits the main maven
repositories from which Coursier installs the dependencies.**

## License ##

This code is licensed under the Business Source License 1.1, see the
[LICENSE](LICENSE) file for details.

## System requirements ##

- Java 11

## Documentation ##

The documentation is located under the `docs` module and ca be build using
the `docs/makeSite` task via `sbt`. Current stable documentation is
automatically made available at https://search-agent.wegtam.com/

## Support ##

If you are interested in paid business support, customised installations or
any other service (e.g. search technology, data integration, etc.) please
contact [Wegtam GmbH](https://www.wegtam.com/).

## Developer guide ##

For development and testing you need to install [sbt](http://www.scala-sbt.org/).
Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details how to to contribute
to the project.

### Design ###

The application is split into several modules to ease concentrating
development on specific parts. Most importantly the `shared` module contains
code which is shared across not only modules but also platforms (currently
the JVM and Javascript).

Next is the `engines` module which depends upon `shared` and provides all
search engine implementations. *Please note that currently every new engine
has to be added to the `all` function within the `SearchEnginesLoader`
object!*

Within the `metasearch` module everything related to performing an actual
"meta search" (querying engines, aggregating results, sorting, ...) is
stored.

The `cli` module contains the command line application which provides simple
search functions for terminals.

For networking there is the `service` module which provides a HTTP API that
can be queried and be used as a backend for building interfaces.

### Tests ###

Tests are included in the project. You can run them via the appropriate sbt tasks
`test` and `it:test`. The latter will execute the integration tests.
Be aware that the integration tests might need a working database.

## Deployment guide ##

TODO

