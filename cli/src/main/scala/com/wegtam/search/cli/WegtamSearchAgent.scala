/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.cli

import cats.effect._
import cats.syntax.all._
import com.monovore.decline._
import com.monovore.decline.effect._
import com.monovore.decline.refined._
import com.typesafe.config.ConfigFactory
import com.wegtam.search.MetaSearch
import com.wegtam.search.cli.ArgumentInstances._
import com.wegtam.search.cli.ConfigInstances._
import com.wegtam.search.common._
import com.wegtam.search.engines.SearchEnginesLoader
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import eu.timepit.refined.types.numeric.PosInt
import org.slf4j.LoggerFactory
import pureconfig._
import sttp.client3.asynchttpclient.fs2.AsyncHttpClientFs2Backend

/** This is the main entry point for the CLI part of the Wegtam Search Agent.
  *
  * To get an overview run it with the `--help` flag. It is intended to execute searches from the command line and
  * return results in a variety of formats.
  */
object WegtamSearchAgent
    extends CommandIOApp(
      name = "Wegtam Search Agent",
      header = "CLI interface for the Wegtam Search Agent meta search engine.",
      version = ""
    ) {
  private final val log = LoggerFactory.getLogger(WegtamSearchAgent.getClass())

  val listEngines  = Opts.flag("list-engines", help = "List all available search engines.")
  val listFormats  = Opts.flag("list-formats", help = "List supported output formats for search results.")
  val listProfiles = Opts.flag("list-profiles", help = "List all configured search profiles.")
  val capabilities = Opts.arguments[SearchEngineCapabilities]()
  val resultFormat =
    Opts.option[SearchResultFormat](
      "format",
      help = "Specify the output format for search results, default: CLI.",
      metavar = "format-name",
      short = "f"
    )
  val searchEngines =
    Opts.options[SearchEngineName](
      "engine",
      help = "Specify a search engine name to be used in the search. This option can be given multiple times.",
      metavar = "engine-name",
      short = "e"
    )
  val searchMode = Opts.option[SearchMode](
    "mode",
    help = "Specify the search mode for engines, default: GENERIC.",
    metavar = "mode-name",
    short = "m"
  )
  val searchProfile = Opts.option[SearchProfileName](
    "profile",
    help = "Specify a search profile to be used.",
    metavar = "profile-name",
    short = "p"
  )
  val searchQuery = Opts.option[String](
    "query",
    help = "The search query which has to be quoted if it contains multiple words.",
    metavar = "string",
    short = "q"
  )
  val searchResults =
    Opts.option[PosInt](
      "results",
      help = "The number of desired results per search engine, default: 10.",
      metavar = "integer",
      short = "r"
    )

  override def main: Opts[IO[ExitCode]] = {
    val config     = IO(ConfigFactory.load(getClass().getClassLoader()))
    val profiles   = config.map(cfg => ConfigSource.fromConfig(cfg).loadOrThrow[SearchProfiles])
    val cores      = PosInt.from(Runtime.getRuntime().availableProcessors()).getOrElse(1: PosInt)
    val allEngines = SearchEnginesLoader.all[IO]()
    (
      listEngines.orFalse,
      listFormats.orFalse,
      listProfiles.orFalse,
      searchEngines.orEmpty,
      resultFormat.withDefault(SearchResultFormat.CLI),
      searchProfile.orNone,
      searchQuery.orNone,
      searchResults.withDefault(10: PosInt)
    ).mapN {
      case (lsEngines, lsFormat, lsProfiles, Nil, _, None, _, _) =>
        for {
          ps <- profiles
          _ <-
            if (lsEngines)
              IO.delay(
                println(
                  allEngines.map(e => s"${e.name.toString()} (${e.capabilities.toList.mkString(", ")})").mkString("\n")
                )
              )
            else IO.pure(())
          _ <- if (lsFormat) IO.delay(println(SearchResultFormat.values.mkString("\n"))) else IO.pure(())
          _ <- if (lsProfiles) IO.delay(println(ps.profiles.mkString)) else IO.pure(())
        } yield ExitCode.Success
      case (_, _, _, engines, format, profile, Some(query), results) =>
        val givenEngines = engines.flatMap(n => allEngines.find(_.name === n))
        for {
          ps            <- profiles
          p             <- IO.delay(profile.flatMap(n => ps.profiles.get(n)))
          es            <- IO.delay(p.map(_.engines.flatMap(n => allEngines.find(_.name === n))).getOrElse(List.empty))
          searchEngines <- IO.delay((givenEngines ::: es).distinct)
          _ <-
            if (searchEngines.isEmpty) IO.delay(log.error("No search engines given by parameters or profile!"))
            else IO.pure(())
          q <- IO.delay(SearchQuery(query, None, results))
          _ <- IO.delay(log.debug("Using {} cpu cores for parallel execution.", cores))
          rs <- AsyncHttpClientFs2Backend.resource[IO]().use { backend =>
            // rs <- HttpClientFs2Backend[IO]().flatMap { backend =>
            val rs = MetaSearch.execute(backend)(cores)(searchEngines)(q)
            MetaSearch.sort(rs)
          }
          _ <- IO.delay(log.debug("Found {} results.", rs.size))
          _ <- IO.delay(println(SearchResultFormatter.formatAll(format)(rs)))
        } yield ExitCode.Success
      case (_, _, _, _, _, _, None, _) =>
        for {
          _ <- IO.delay(println("You must specify a search query!"))
        } yield ExitCode.Error
    }
  }
}
