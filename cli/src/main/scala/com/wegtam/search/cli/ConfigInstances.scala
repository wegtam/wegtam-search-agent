/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.cli

import cats.syntax.all._
import com.wegtam.search.common._
import eu.timepit.refined.auto._
import eu.timepit.refined.pureconfig._
import pureconfig._
import pureconfig.ConvertHelpers._
import pureconfig.configurable._
import pureconfig.generic.semiauto._
import pureconfig.error._

/** Instances for pureconfig to be able to load the necessary configuration files.
  */
object ConfigInstances {

  implicit val searchModeReader: ConfigReader[SearchMode] = ConfigReader.fromString(s =>
    SearchMode.withNameEither(s).leftMap(_ => CannotConvert(s, "SearchMode", s"$s is not a SearchMode"))
  )

  implicit val searchProfileReader: ConfigReader[SearchProfile] = deriveReader[SearchProfile]

  implicit val searchProfilesMapReader: ConfigReader[Map[SearchProfileName, SearchProfile]] =
    genericMapReader[SearchProfileName, SearchProfile](optF[SearchProfileName](s => SearchProfileName.from(s).toOption))

  implicit val searchProfilesReader: ConfigReader[SearchProfiles] = deriveReader[SearchProfiles]

}
