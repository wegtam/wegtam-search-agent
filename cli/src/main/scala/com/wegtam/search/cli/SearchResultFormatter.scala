/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.cli

import com.wegtam.search.cli.SearchResultFormat._
import com.wegtam.search.common._
import io.circe.syntax._

import scala.collection.immutable._

object SearchResultFormatter {

  def format(f: SearchResultFormat)(r: SearchResult): String =
    f match {
      case CLI =>
        s"${r.url.map(_.toString()).getOrElse("")} (${r.searchEngines.size.toString()}, ${r.number.toString()})"
      case CSV =>
        s"""${r.number.toString()},"${r.searchEngines
          .mkString(";")}",${r.url.map(_.toString()).getOrElse("")},"${r.title
          .map(_.toString())
          .getOrElse("")}","${r.description.map(_.toString()).getOrElse("")}""""
      case HTML =>
        val description = r.description.map(_.toString()).getOrElse("")
        val number      = r.number.toString()
        val title       = r.title.map(_.toString()).getOrElse("")
        val url         = r.url.map(_.toString()).getOrElse("")
        s"""<div class="search-result">
        |  <div class="result-title"><a class="result-link" href="$url">$title</a></div>
        |  <div class="result-url"><a class="result-link" href="$url">$title</a></div>
        |  <div class="result-description">$description</div>
        |  <div class="result-engines">
        |    ${r.searchEngines.map(_.toString()).mkString(", ")}
        |  </div>
        |  <div class="result-number">$number</div>
        |</div>""".stripMargin
      case JSON =>
        r.asJson.spaces2
      case VERBOSE =>
        List(
          s"""Number:             ${r.number.toString()}""",
          s"""Search engines:     ${r.searchEngines.mkString(", ")}""",
          s"""Title:              ${r.title.map(_.toString()).getOrElse("")}""",
          s"""Description:        ${r.description.map(_.toString()).getOrElse("")}""",
          s"""Authors:            ${r.authors.mkString(", ")}""",
          s"""Published:          ${r.published.map(_.toString()).getOrElse("")}""",
          s"""Publisher:          ${r.publisher.mkString(", ")}""",
          s"""Publishing details: ${r.publishingDetails.mkString(", ")}""",
          s"""Image URL:          ${r.imageUrl.map(_.toString()).getOrElse("")}""",
          s"""URL:                ${r.url.map(_.toString()).getOrElse("")}""",
          """-- """
        ).mkString("\n")
    }

  def formatAll(f: SearchResultFormat)(rs: Seq[SearchResult]): String = {
    val fmt: SearchResult => String = format(f)(_)
    f match {
      case CLI     => rs.map(fmt).mkString("\n")
      case CSV     => rs.map(fmt).mkString("\n")
      case HTML    => rs.map(fmt).mkString("\n")
      case JSON    => rs.map(fmt).mkString("[", ",", "]")
      case VERBOSE => rs.map(fmt).mkString("\n")
    }
  }

}
