/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.cli

import cats._
import cats.data.{ Validated, ValidatedNel }
import cats.syntax.all._
import com.monovore.decline.Argument

/** A list of formats in which search results can be rendered.
  */
sealed trait SearchResultFormat extends Product with Serializable

object SearchResultFormat extends {
  private val mappings: Map[String, SearchResultFormat] = Map(
    "CLI"     -> CLI,
    "CSV"     -> CSV,
    "HTML"    -> HTML,
    "JSON"    -> JSON,
    "VERBOSE" -> VERBOSE
  )

  implicit val eqSearchResultFormat: Eq[SearchResultFormat] = Eq.fromUniversalEquals

  implicit val readSearchResultFormat: Argument[SearchResultFormat] = new Argument[SearchResultFormat] {
    override def defaultMetavar: String = "search result format"

    override def read(string: String): ValidatedNel[String, SearchResultFormat] =
      SearchResultFormat.withNameOption(string) match {
        case None    => Validated.invalidNel(s"Invalid SearchResultFormat: $string")
        case Some(c) => Validated.valid(c)
      }
  }

  implicit val showSearchResultFormat: Show[SearchResultFormat] =
    Show.show(m => mappings.find(_._2 === m).map(_._1).getOrElse(m.getClass().getSimpleName()))

  val values = mappings.values.toList

  /** Return either the `SearchResultFormat` with the given name or an error message.
    *
    * @param name
    *   The name of a search result format.
    * @return
    *   Either the search result format or an error message.
    */
  def withNameEither(name: String): Either[String, SearchResultFormat] =
    mappings.get(name) match {
      case Some(m) => Right(m)
      case None    => Left(s"No such SearchResultFormat: $name")
    }

  /** Return an option to `SearchResultFormat` with the given name.
    *
    * @param name
    *   The name of a search result format.
    * @return
    *   An option to the desired search result format which may be none.
    */
  def withNameOption(name: String): Option[SearchResultFormat] = mappings.get(name)

  /** Intended for basic command line usage, less information, parseable by scripts.
    */
  case object CLI extends SearchResultFormat

  /** Contains more information.
    */
  case object CSV extends SearchResultFormat

  /** Simple HTML rendered result for embedding.
    */
  case object HTML extends SearchResultFormat

  /** Verbose JSON output for further parsing.
    */
  case object JSON extends SearchResultFormat

  /** Pretty verbose output intended for humans to read.
    */
  case object VERBOSE extends SearchResultFormat

}
