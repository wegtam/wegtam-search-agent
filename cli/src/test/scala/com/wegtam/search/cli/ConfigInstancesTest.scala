/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.cli

import cats.effect._
import com.typesafe.config.ConfigFactory
import com.wegtam.search.cli.ConfigInstances._
import com.wegtam.search.common._
import com.wegtam.search.engines._
import eu.timepit.refined.auto._
import pureconfig._

class ConfigInstancesTest extends munit.CatsEffectSuite {
  val config = ResourceSuiteLocalFixture(
    "config",
    Resource.make(
      IO.blocking(
        ConfigFactory.load(getClass().getClassLoader())
      )
    )(_ => IO.unit)
  )

  override def munitFixtures = List(config)

  test("loading SearchProfiles must work") {
    val profiles = IO(ConfigSource.fromConfig(config()).loadOrThrow[SearchProfiles])
    val expected: Map[SearchProfileName, SearchProfile] = Map(
      (
        "generic",
        SearchProfile(
          engines = List(Bing.ENGINE_NAME, DuckDuckGo.ENGINE_NAME, Google.ENGINE_NAME),
          mode = Option(SearchMode.GENERIC)
        )
      ),
      (
        "science",
        SearchProfile(
          engines = List(ArXiv.ENGINE_NAME, NationalArchivesUK.ENGINE_NAME, NationalArchivesUSA.ENGINE_NAME),
          mode = None
        )
      )
    )
    profiles.map(_.profiles).assertEquals(expected)
  }

}
