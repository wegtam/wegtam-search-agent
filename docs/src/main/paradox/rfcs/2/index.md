---
number: 2
shortname: 2/ENGINE
name: Search Engine API
status: raw
editor: Jens Grassel <jens@wegtam.com>
contributors:
---

# 2/ENGINE (Search Engine API)

![RAW](.../raw.svg)

## License

Copyright (c) 2021 the Editor and the Contributors.

This specification is subject to the terms of the Mozilla Public License, v.
2.0. If a copy of the MPL was not distributed with this file, You can obtain
one at http://mozilla.org/MPL/2.0/.

## Language

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
"SHOULD", "SHOULD NOT", "RECOMMENDED",  "MAY", and "OPTIONAL" in this
document are to be interpreted as described in @extref[RFC 2119](rfc:2119).

## Overview

### Use Case

To allow easy addition of new search engines the API of the base class
(@scaladoc[SearchEngine](com.wegtam.search.engines.SearchEngine)) SHOULD be
lightweight and straightforward. A developer SHOULD be able to implement
their own engine quickly by following the appropriate documents and source
code.

### Design of the API

The API for the
@scaladoc[SearchEngine](com.wegtam.search.engines.SearchEngine) class
exposes several functions which have to be implemented by an actual search
engine class.

## Technical Specification

1. The engine MUST be implemented in the `engines` modules in the
   `com.wegtam.search.engines` package.
2. The engine MUST implement the
   @scaladoc[SearchEngine](com.wegtam.search.engines.SearchEngine)
   interface.
3. The parser of the engine MUST implement the
   @scaladoc[SearchEngineParser](com.wegtam.search.engines.SearchEngineParser)
   interface.
4. The `name` of the implemented engine MUST be unique.
5. The implemented engine MUST be added to the `all` function of the
   @scaladoc[SearchEnginesLoader](com.wegtam.search.engines.SearchEnginesLoader$)
   object.

### SearchEngine Interface

The interface MUST provide the following functions which SHALL be
implemented by a concrete search engine implementation:

1. `capabilities`
    - MUST return the capabilities that the search engine supports and that    
      are implemented.
2. `modes`
    - MUST return all supported and implemented search modes of the engine.   
      This list MUST never be empty because at least one mode is supported.
3. `name`
    - MUST return the unique search engine name.
4. `parser`
    - MUST return the implementation of the parser for the search engine.
5. `search`
    - MUST perform the actual search and return a stream with the parsed 
      results.

