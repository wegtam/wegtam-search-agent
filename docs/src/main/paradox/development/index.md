@@@ index

* [Module Dependencies](dependencies.md)
* [Add an engine](adding-a-search-engine.md)

@@@


# Development

The source code is structured into some sub modules to allow focusing on
specific task more easily.

| Module     | Notes                                                                                  |
| ---------- | -----                                                                                  |
| cli        | The command line application to instrument searches.                                   |
| docs       | All documentation which is not embedded into the source code.                          |
| engines    | Implementation of various search engines.                                              |
| metasearch | Implementation of functionalities for meta search.                                     |
| service    | Implementation of the service module.                                                  |
| shared     | Shared code which is needed by various modules and across platforms (JVM, Javascript). |

TODO

