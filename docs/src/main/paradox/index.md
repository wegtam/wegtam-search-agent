@@@ index

* [API](api.md)
* [Development](development/index.md)
* [RFCs](rfcs/index.md)

@@@

# Wegtam Search Agent

Your personal search agent for the information age.

The Wegtam Search Agent provides a meta search upon a variety of search engines and aggregates their results.

## Support

If you are interested in paid business support, customised installations or
any other service (e.g. search technology, data integration, etc.) please
contact [Wegtam GmbH](https://www.wegtam.com/).

## Language

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
"SHOULD", "SHOULD NOT", "RECOMMENDED",  "MAY", and "OPTIONAL" in this
document are to be interpreted as described in @extref[RFC 2119](rfc:2119).

