/*
 * Copyright (c) 2008 - 2021 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search

import com.wegtam.search.common._
import io.circe.refined._
import sttp.model._
import sttp.tapir._
import sttp.tapir.generic.auto._
import sttp.tapir.json.circe._

object Service {

  val root: Endpoint[Unit, Unit, StatusCode, String, Any] =
    endpoint.get.in("/").errorOut(statusCode).out(htmlBodyUtf8)

  val listEngines: Endpoint[Unit, Unit, StatusCode, List[SearchEngineName], Any] =
    endpoint.get.in("engines").errorOut(statusCode).out(jsonBody[List[SearchEngineName]])

}
