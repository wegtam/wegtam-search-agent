// *****************************************************************************
// Projects
// *****************************************************************************

addCommandAlias("check", "compile:scalafix --check; test:scalafix --check")
addCommandAlias("fix", "compile:scalafix; test:scalafix; scalafmtSbt; scalafmtAll")

lazy val wegtam =
  project
    .in(file("."))
    .settings(settings)
    .settings(
      name := "wegtam-search-agent",
      crossScalaVersions := Nil, // Workaround for "Repository for publishing is not specified." error
      publish := {},
      publishLocal := {}
    )
    .aggregate(cli, engines, metasearch, shared.jvm, shared.js)

lazy val docs =
  project
    .in(file("docs"))
    .enablePlugins(ParadoxSitePlugin, ScalaUnidocPlugin)
    .settings(settings)
    .settings(
      name := "search-agent-docs",
      autoAPIMappings := true,
      paradoxProperties ++= Map(
        "extref.rfc.base_url"      -> "http://tools.ietf.org/html/rfc%s",
	"image.base_url"           -> ".../images",
        "project.description"      -> "The Wegtam Search Agent provides a meta search upon a variety of search engines and aggregates their results.",
        "scaladoc.base_url"        -> ".../api/",
	"snip.cli.base_dir"        -> (cli / sourceDirectory).value.getAbsolutePath,
	"snip.engines.base_dir"    -> (engines / sourceDirectory).value.getAbsolutePath,
	"snip.metasearch.base_dir" -> (metasearch / sourceDirectory).value.getAbsolutePath,
	"snip.service.base_dir"    -> (service/ sourceDirectory).value.getAbsolutePath,
        "snip.project.base_dir"    -> (wegtam / baseDirectory).value.getAbsolutePath
      ),
      paradoxTheme := Some(builtinParadoxTheme("generic")),
      ScalaUnidoc / siteSubdirName := "api",
      ScalaUnidoc / unidoc / unidocProjectFilter := inAnyProject -- inProjects(shared.js),
      addMappingsToSiteDir(ScalaUnidoc / packageDoc / mappings, ScalaUnidoc / siteSubdirName),
    )

lazy val shared =
  crossProject(JSPlatform, JVMPlatform)
    .in(file("shared"))
    .enablePlugins(AutomateHeaderPlugin, SiteScaladocPlugin)
    .settings(settings)
    .settings(
      automateHeaderSettings(Compile, Test),
      name := "search-agent-shared",
      libraryDependencies ++= Seq(
        "org.typelevel"                 %%% "cats-core"             % Version.cats,
        "io.circe"                      %%% "circe-core"            % Version.circe,
        "io.circe"                      %%% "circe-generic"         % Version.circe,
        "io.circe"                      %%% "circe-refined"         % Version.circe,
        "io.circe"                      %%% "circe-parser"          % Version.circe,
        "com.wegtam"                    %%% "countries"             % Version.countries,
        "eu.timepit"                    %%% "refined"               % Version.refined,
        "eu.timepit"                    %%% "refined-cats"          % Version.refined,
        "com.softwaremill.sttp.client3" %%% "core"                  % Version.sttp,
        "org.typelevel"                 %%% "discipline-munit"      % Version.disciplineMunit % Test,
        "org.scalameta"                 %%% "munit"                 % Version.munit           % Test,
        "org.scalameta"                 %%% "munit-scalacheck"      % Version.munit           % Test,
        "eu.timepit"                    %%% "refined-scalacheck"    % Version.refined         % Test,
        "org.scalacheck"                %%% "scalacheck"            % Version.scalaCheck      % Test,
      )
    )
    .jvmSettings(
      libraryDependencies ++= Seq(
        library.jsoup,
        "org.scala-js" %% "scalajs-stubs" % "1.1.0" % Provided
      )
    )
    .jsSettings(
      libraryDependencies ++= Seq(
        "io.github.cquiroz" %%% "scala-java-time"      % Version.scalaJavaTime,
        "io.github.cquiroz" %%% "scala-java-time-tzdb" % Version.scalaJavaTime,
      ),
      scalaJSUseMainModuleInitializer := false,
      Test / scalaJSLinkerConfig ~= { _.withModuleKind(ModuleKind.CommonJSModule) },
      Test / scalaJSUseMainModuleInitializer := false
    )

lazy val engines =
  project
    .in(file("engines"))
    .enablePlugins(AutomateHeaderPlugin, SiteScaladocPlugin)
    .dependsOn(shared.jvm)
    .configs(IntegrationTest)
    .settings(
      name := "search-agent-engines"
    )
    .settings(settings)
    .settings(
      Defaults.itSettings,
      automateHeaderSettings(IntegrationTest),
      IntegrationTest / console / scalacOptions --= Seq(
        "-Xfatal-warnings",
        "-Ywarn-unused-import",
        "-Ywarn-unused:implicits",
        "-Ywarn-unused:imports",
        "-Ywarn-unused:locals",
        "-Ywarn-unused:params",
        "-Ywarn-unused:patvars",
        "-Ywarn-unused:privates"
      ),
      IntegrationTest / parallelExecution := false
    )
    .settings(
      libraryDependencies ++= Seq(
        library.fs2Core,
        library.fs2IO,
        library.jsoup,
        library.logback,
        library.pureConfig,
        library.refinedCats,
        library.refinedCore,
        library.refinedPureConfig,
        library.sttpCore,
        library.sttpFs2,
	library.enumeratumCats       % IntegrationTest,
	library.enumeratumScalaCheck % IntegrationTest,
        library.munit                % IntegrationTest,
        library.munitCatsEffect      % IntegrationTest,
        library.munitScalaCheck      % IntegrationTest,
        library.refinedScalaCheck    % IntegrationTest,
        library.scalaCheck           % IntegrationTest,
	library.enumeratumCats       % Test,
	library.enumeratumScalaCheck % Test,
        library.munit                % Test,
        library.munitCatsEffect      % Test,
        library.munitScalaCheck      % Test,
        library.refinedScalaCheck    % Test,
        library.scalaCheck           % Test,
      )
    )

lazy val metasearch =
  project
    .in(file("metasearch"))
    .enablePlugins(AutomateHeaderPlugin, SiteScaladocPlugin)
    .dependsOn(engines, shared.jvm)
    .settings(
      name := "search-agent-metasearch"
    )
    .settings(settings)

lazy val cli =
  project
    .in(file("cli"))
    .enablePlugins(
      AutomateHeaderPlugin,
      JavaServerAppPackaging,
      SiteScaladocPlugin,
      UniversalPlugin
    )
    .dependsOn(engines, metasearch, shared.jvm)
    .settings(
      name := "search-agent-cli"
    )
    .settings(settings)
    .settings(
      libraryDependencies ++= Seq(
        library.decline,
        library.declineEffect,
        library.declineRefined,
        library.logback,
        library.pureConfig,
        library.refinedPureConfig,
        library.munit                % Test,
        library.munitCatsEffect      % Test,
        library.munitScalaCheck      % Test,
        library.refinedScalaCheck    % Test,
        library.scalaCheck           % Test,
      ),
      executableScriptName := "wsa-cli",
      maintainer := "Wegtam GmbH <devops@wegtam.com>",
      packageSummary := "Wegtam Search Agent - CLI",
      packageDescription := "A command line interface for the Wegtam Search Agent.",
    )

lazy val service =
  project
    .in(file("service"))
    .enablePlugins(AutomateHeaderPlugin, SiteScaladocPlugin)
    .dependsOn(engines, metasearch, shared.jvm)
    .settings(
      name := "search-agent-service"
    )
    .settings(settings)
    .settings(
      libraryDependencies ++= Seq(
        library.circeCore,
        library.circeGeneric,
        library.circeParser,
        library.circeRefined,
        library.decline,
        library.declineEffect,
        library.declineRefined,
        library.logback,
        library.tapirCore,
        library.tapirCirce,
        library.tapirHttp4s,
        library.tapirRefined,
      )
    )

// *****************************************************************************
// Library dependencies
// *****************************************************************************

lazy val Version = new {
  val cats            = "2.7.0"
  val catsEffect      = "3.0.1"
  val circe           = "0.14.1"
  val countries       = "1.0.0"
  val cryptobits      = "1.3"
  val decline         = "2.2.0"
  val disciplineMunit = "1.0.9"
  val enumeratum      = "1.7.0"
  val flyway          = "7.5.4"
  val fs2             = "3.2.4"
  val http4s          = "0.21.19"
  val jsoup           = "1.14.3"
  val jBCrypt         = "0.4.3"
  val logback         = "1.2.10"
  val munit           = "0.7.29"
  val munitCatsEffect = "1.0.7"
  val postgresql      = "42.2.18"
  val pureConfig      = "0.17.1"
  val refined         = "0.9.28"
  val scalaCheck      = "1.15.4"
  val scalaJavaTime   = "2.3.0" // Needed (with tzdb) for java.time support in Scala.js!
  val scalaTags       = "0.10.0"
  val sttp            = "3.3.18"
  val tapir           = "0.19.0"
  val webjarBootstrap = "4.5.3"
  val webjarJQuery    = "3.5.1"
  val webjarJQueryUI  = "1.12.1"
  val webjarPopperJS  = "1.16.0"
}

lazy val library =
  new {
    val catsCore             = "org.typelevel"                 %% "cats-core"                     % Version.cats
    val catsEffect           = "org.typelevel"                 %% "cats-effect"                   % Version.catsEffect
    val circeCore            = "io.circe"                      %% "circe-core"                    % Version.circe
    val circeGeneric         = "io.circe"                      %% "circe-generic"                 % Version.circe
    val circeRefined         = "io.circe"                      %% "circe-refined"                 % Version.circe
    val circeParser          = "io.circe"                      %% "circe-parser"                  % Version.circe
    val countries            = "com.wegtam"                    %% "countries"                     % Version.countries
    val cryptobits           = "org.reactormonk"               %% "cryptobits"                    % Version.cryptobits
    val decline              = "com.monovore"                  %% "decline"                       % Version.decline
    val declineEffect        = "com.monovore"                  %% "decline-effect"                % Version.decline
    val declineRefined       = "com.monovore"                  %% "decline-refined"               % Version.decline
    val enumeratumCats       = "com.beachape"                  %% "enumeratum-cats"               % Version.enumeratum
    val enumeratumScalaCheck = "com.beachape"                  %% "enumeratum-scalacheck"         % Version.enumeratum
    val flywayCore           = "org.flywaydb"                  %  "flyway-core"                   % Version.flyway
    val fs2Core              = "co.fs2"                        %% "fs2-core"                      % Version.fs2
    val fs2IO                = "co.fs2"                        %% "fs2-io"                        % Version.fs2
    val http4sCirce          = "org.http4s"                    %% "http4s-circe"                  % Version.http4s
    val http4sDsl            = "org.http4s"                    %% "http4s-dsl"                    % Version.http4s
    val jsoup                = "org.jsoup"                     %  "jsoup"                         % Version.jsoup
    val logback              = "ch.qos.logback"                %  "logback-classic"               % Version.logback
    val munit                = "org.scalameta"                 %% "munit"                         % Version.munit
    val munitCatsEffect      = "org.typelevel"                 %% "munit-cats-effect-3"           % Version.munitCatsEffect
    val munitScalaCheck      = "org.scalameta"                 %% "munit-scalacheck"              % Version.munit
    val postgresql           = "org.postgresql"                %  "postgresql"                    % Version.postgresql
    val pureConfig           = "com.github.pureconfig"         %% "pureconfig"                    % Version.pureConfig
    val refinedCore          = "eu.timepit"                    %% "refined"                       % Version.refined
    val refinedCats          = "eu.timepit"                    %% "refined-cats"                  % Version.refined
    val refinedPureConfig    = "eu.timepit"                    %% "refined-pureconfig"            % Version.refined
    val refinedScalaCheck    = "eu.timepit"                    %% "refined-scalacheck"            % Version.refined
    val scalaCheck           = "org.scalacheck"                %% "scalacheck"                    % Version.scalaCheck
    val scalaTags            = "com.lihaoyi"                   %% "scalatags"                     % Version.scalaTags
    val sttpCore             = "com.softwaremill.sttp.client3" %% "core"                          % Version.sttp
    val sttpFs2              = "com.softwaremill.sttp.client3" %% "async-http-client-backend-fs2" % Version.sttp
    val tapirCats            = "com.softwaremill.sttp.tapir"   %% "tapir-cats"                    % Version.tapir
    val tapirCirce           = "com.softwaremill.sttp.tapir"   %% "tapir-json-circe"              % Version.tapir
    val tapirCore            = "com.softwaremill.sttp.tapir"   %% "tapir-core"                    % Version.tapir
    val tapirHttp4s          = "com.softwaremill.sttp.tapir"   %% "tapir-http4s-server"           % Version.tapir
    val tapirOpenApiDocs     = "com.softwaremill.sttp.tapir"   %% "tapir-openapi-docs"            % Version.tapir
    val tapirOpenApiYaml     = "com.softwaremill.sttp.tapir"   %% "tapir-openapi-circe-yaml"      % Version.tapir
    val tapirRefined         = "com.softwaremill.sttp.tapir"   %% "tapir-refined"                 % Version.tapir
    val tapirSwaggerUi       = "com.softwaremill.sttp.tapir"   %% "tapir-swagger-ui-http4s"       % Version.tapir
    val webjarBootstrap      = "org.webjars"                   %  "bootstrap"                     % Version.webjarBootstrap
    val webjarJQuery         = "org.webjars"                   %  "jquery"                        % Version.webjarJQuery
    val webjarJQueryUI       = "org.webjars"                   %  "jquery-ui"                     % Version.webjarJQueryUI
    val webjarPopperJS       = "org.webjars"                   %  "popper.js"                     % Version.webjarPopperJS
  }

// *****************************************************************************
// Settings
// *****************************************************************************

lazy val settings =
  commonSettings ++
  publishSettings ++
  resolverSettings ++
  scoverageSettings

val MUnitFramework = new TestFramework("munit.Framework")

lazy val currentYear: Int = java.time.OffsetDateTime.now().getYear

lazy val commonSettings =
  Seq(
    scalaVersion := "2.13.8",
    organization := "com.wegtam",
    organizationName := "Wegtam GmbH",
    startYear := Some(2008),
    headerLicense := Some(HeaderLicense.Custom(
      s"""|Copyright (c) 2008 - $currentYear Wegtam GmbH
          |
          |Business Source License 1.1 - See file LICENSE for details!
          |
          |Change Date:    2024-06-21
          |Change License: Mozilla Public License Version 2.0
          |""".stripMargin
    )),
    addCompilerPlugin("com.olegpy"    %% "better-monadic-for" % "0.3.1"),
    addCompilerPlugin("org.typelevel" %% "kind-projector"     % "0.10.3"),
    scalacOptions ++= Seq(
      "-deprecation",
      "-explaintypes",
      "-feature",
      "-language:_",
      "-unchecked",
      "-Xcheckinit",
      "-Xfatal-warnings",  // Disable if using twirl and unused:imports!
      "-Xlint:adapted-args",
      "-Xlint:constant",
      "-Xlint:delayedinit-select",
      "-Xlint:doc-detached",
      "-Xlint:inaccessible",
      "-Xlint:infer-any",
      "-Xlint:missing-interpolator",
      "-Xlint:nullary-unit",
      "-Xlint:option-implicit",
      "-Xlint:package-object-classes",
      "-Xlint:poly-implicit-overload",
      "-Xlint:private-shadow",
      "-Xlint:stars-align",
      "-Xlint:type-parameter-shadow",
      "-Ywarn-dead-code",
      "-Ywarn-extra-implicit",
      "-Ywarn-numeric-widen",
      "-Ywarn-unused:implicits",
      "-Ywarn-unused:imports", // May produce false positives with twirl templates.
      "-Ywarn-unused:locals",
      "-Ywarn-unused:params",
      "-Ywarn-unused:patvars",
      "-Ywarn-unused:privates",
      "-Ywarn-value-discard",
      "-Ycache-plugin-class-loader:last-modified",
      "-Ycache-macro-class-loader:last-modified",
      "-Yrangepos", // Needed to make `clue` work correctly (munit).
    ),
    Compile / console / scalacOptions --= Seq(
      "-Xfatal-warnings",
      "-Ywarn-unused-import",
      "-Ywarn-unused:implicits",
      "-Ywarn-unused:imports",
      "-Ywarn-unused:locals",
      "-Ywarn-unused:params",
      "-Ywarn-unused:patvars",
      "-Ywarn-unused:privates"
    ),
    testFrameworks += MUnitFramework,
    Test / parallelExecution := false,
    //Test / testOptions += Tests.Argument(MUnitFramework, "--exclude-tags=OnlineTest"),
    Compile / compile / wartremoverWarnings ++= Warts.unsafe.filterNot(_ == Wart.Any), // Disable the "Any" wart due to too many false positives.
    Test / console / scalacOptions --= Seq(
      "-Xfatal-warnings",
      "-Ywarn-unused-import",
      "-Ywarn-unused:implicits",
      "-Ywarn-unused:imports",
      "-Ywarn-unused:locals",
      "-Ywarn-unused:params",
      "-Ywarn-unused:patvars",
      "-Ywarn-unused:privates"
    )
)

lazy val publishSettings =
  Seq(
    credentials += Credentials(Path.userHome / ".sbt" / "sonatype_credentials"),
    developers ++= List(
      Developer(
        "wegtam",
        "Wegtam GmbH",
        "tech@wegtam.com",
        url("https://www.wegtam.com")
      ),
      Developer(
        "jan0sch",
        "Jens Grassel",
        "jens@wegtam.com",
        url("https://www.jan0sch.de")
      )
    ),
    ThisBuild / dynverSeparator := "-",
    ThisBuild / dynverSonatypeSnapshots := true,
    homepage := Option(url("https://codeberg.org/wegtam/wegtam-search-agent")),
    licenses += ("BSL-1.1", url("https://codeberg.org/wegtam/wegtam-search-agent/src/branch/main/LICENSE")),
    pgpSigningKey := Option(sys.env.getOrElse("PUBLISH_SIGNING_KEY", "633EA119CC2D5F249A0F409A002841124A42559E")),
    pomIncludeRepository := (_ => false),
    Test / publishArtifact := false,
    publishMavenStyle := true,
    publishTo := {
      val nexus = "https://oss.sonatype.org/"
      if (isSnapshot.value)
        Some("snapshots".at(nexus + "content/repositories/snapshots"))
      else
        Some("releases".at(nexus + "service/local/staging/deploy/maven2"))
    },
    publish := (publish dependsOn (Test / test)).value,
    scmInfo := Option(ScmInfo(
      url("https://codeberg.org/wegtam/wegtam-search-agent"),
      "git@codeberg.org:wegtam/wegtam-search-agent.git"
    )),
    ThisBuild / versionScheme := Option("semver-spec")
  )

lazy val resolverSettings =
  Seq(
    resolvers ++= Seq(
      Resolver.sonatypeRepo("releases"),
      Resolver.sonatypeRepo("snapshots")
    )
  )

lazy val scoverageSettings =
  Seq(
    coverageMinimumStmtTotal := 60,
    coverageFailOnMinimum := false,
    coverageHighlighting := true
  )

