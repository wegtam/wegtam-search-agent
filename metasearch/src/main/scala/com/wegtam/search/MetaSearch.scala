/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search

import cats.effect._
import cats.syntax.all._
import com.wegtam.search.common._
import com.wegtam.search.engines._
import eu.timepit.refined.auto._
import eu.timepit.refined.types.numeric.PosInt
import fs2.Stream
import org.slf4j.LoggerFactory
import sttp.client3.SttpBackend

object MetaSearch {
  private final val log = LoggerFactory.getLogger(MetaSearch.getClass())

  /** Log the given error and return an empty stream of search results.
    *
    * @param e
    *   An exception thrown during the metasearch.
    * @return
    *   An empty stream to allow proceeding the search.
    */
  private def handleAndLogError[F[_]](e: Throwable): Stream[F, SearchResult] = {
    log.error("Error during metasearch!", e)
    Stream.empty
  }

  /** Sort the given stream of search results according to our specifications:
    *
    *   1. Results which are returned by more search engines are ranked higher. 2. Results which equal in the number of
    *      engines are ranked by their weight.
    *
    * For this operation the stream will be materialised (`.compile.toList`) and analysed to find and merge equal
    * results from different search engines.
    *
    * @param rs
    *   A stream of search results which will be materialised to sort it!
    * @return
    *   A sorted sequence of search results.
    */
  def sort[F[_]: Sync](rs: Stream[F, SearchResult]): F[IndexedSeq[SearchResult]] =
    for {
      mat <- rs.compile.toList
      _   <- Sync[F].delay(log.debug("Going to sort {} results.", mat.length))
      (res, dup) <- Sync[F].delay(mat.foldLeft((Vector.empty[SearchResult], Vector.empty[Option[SearchResult]])) {
        (acc, r) =>
          val (a, b) = acc
          // FIXME Why the heck does `_ === r` not work here?!?
          if (a.exists(_.url === r.url))
            (a, b :+ Option(r))
          else
            (a :+ r, b :+ None)
      })
      vec <- Sync[F].delay(res.zip(dup).map {
        case (r, Some(d)) => r.combine(d)
        case (r, None)    => r
      })
      _ <- Sync[F].delay(log.debug("Prepared for sorting ({} entries).", vec.length))
    } yield vec.sorted

  /** Execute a metasearch using the given list of search engines.
    *
    * @param backend
    *   A provided backend for HTTP operations.
    * @param maxParallel
    *   The maximum number of inner threads to use for the search engines.
    * @param es
    *   A list of search engines which shall be queried.
    * @param q
    *   The search query.
    * @return
    *   A stream of search results extracted from all given search engines.
    */
  def execute[F[_]: Concurrent](
      backend: SttpBackend[F, sttp.capabilities.fs2.Fs2Streams[F] with sttp.capabilities.WebSockets]
  )(maxParallel: PosInt)(es: List[SearchEngine[F]])(q: SearchQuery): Stream[F, SearchResult] = {
    log.debug("Going to start a meta search using the following search engines: {}", es.map(_.name).mkString(", "))
    implicit val b                      = backend
    val ses: Stream[F, SearchEngine[F]] = Stream.emits(es)
    ses.map(_.search(q).handleErrorWith(e => handleAndLogError(e))).parJoin(maxParallel)
  }

}
